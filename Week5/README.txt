Week 5 README.txt
Thomas Elliott

Please note, that all in class work has been stored in InClassPracticals Directory and organised by practical. These include many files which were not scripts and therefore does not organise in the normal Code, Data, Results format. This excludes Spatial Methods Practical which was an R script.

######Code

clifford.test.R
Clifford test function required Spatial_Methods_Practical.R which loads variables to calculate the corrections.

CMEE_Week5_Assessment.py
Python script that automates the Merging raters, format conversion and zonal section of Practical 1.

Spatial_Methods_Practical.R
R script that looks at the problems of fitting statistical models to spatial data. Includes code to Correlations and spatial data, Measuring spatial autocorrelation, Autoregressive models, Correlograms

######Data

avian_richness.tif
GeoTIFF file raster file variables for the avian richness across the Afrotropics used for Spatial_Methods_Practical.R

biol_15.tif
Used for CMEE_Week5_Assessment.py: Raster Data set 

biol_16.tif
Used for CMEE_Week5_Assessment.py:Raster Data set 

biol12_15.tif
Used for CMEE_Week5_Assessment.py:Raster Data set 

biol12_16.tif
Used for CMEE_Week5_Assessment.py:Raster Data set 

elev.tif
GeoTIFF file raster file variables for mean elevation used for Spatial_Methods_Practical.R

g250_06.tif
Used for CMEE_Week5_Assessment.py: 2006 CORINE land cover dataset for the EU: it contains land cover classes, so the integer values in the dataset are category codes.

mean_aet.tif
GeoTIFF file raster file variables for the annual actual evapotranspiration used for Spatial_Methods_Practical.R

mean_temp.tif
GeoTIFF file raster file variables for the average annual temperature used for Spatial_Methods_Practical.R


######Result

zonalstats.csv 
Output from CMEE_Week5_Assessment.py this csv file includes climatic values for both bio1 and bio12 within each land cover class.


######InClassPracticals

GIS_Practical_1
All files and results from GIS practical 1, Introduction to QGIS Practical, with saved QGIS projects and data and output required for these

LandUseModelling
All files and results from GIS practical 3, Modelling Land Use and Land Cover Change using StocModLCC

MaxentPractical
All files and results from GIS practical 2, Species Distribution Modelling with Maxent. This includes species data, maxentoutput and plots
