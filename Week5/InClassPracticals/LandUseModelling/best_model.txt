
Call:
glm(formula = factor(train.data[, names(train.data) == "change_lc"]) ~ 
    ., family = binomial, data = train.data[, input.glm], na.action = na.exclude)

Deviance Residuals: 
    Min       1Q   Median       3Q      Max  
-1.0836  -0.2592  -0.1507  -0.0831   3.9377  

Coefficients:
              Estimate Std. Error z value Pr(>|z|)    
(Intercept) -1.538e+00  8.540e-02 -18.008  < 2e-16 ***
droads2     -2.737e-04  1.902e-05 -14.395  < 2e-16 ***
dtowns2     -3.796e-05  3.394e-06 -11.184  < 2e-16 ***
pareas21    -3.338e-01  5.926e-02  -5.632 1.78e-08 ***
pop2         1.948e-01  5.014e-02   3.885 0.000103 ***
neibs0      -1.283e-01  5.023e-03 -25.533  < 2e-16 ***
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

(Dispersion parameter for binomial family taken to be 1)

    Null deviance: 13245  on 63340  degrees of freedom
Residual deviance: 11806  on 63335  degrees of freedom
AIC: 11818

Number of Fisher Scoring iterations: 8

