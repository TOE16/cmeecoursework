#!/usr/bin/python

""" This program imports JCR Reports. Reads file. Searches scopus with API with search terms. Retrieves all Electronic IDs (unique to published article) for the journals included in the JCR report"""

__author__ = 'Thomas ELliott (t.elliott@imperial.ac.uk)'

__version__ = '0.0.1'

# imports
import csv
# Scopus Package has been altered to change the ouput directory to Week8 Data folder
from scopus.scopus_search import ScopusSearch
import os
import glob

# List of all JCR reports in Journal_Info file
path = '../Data/Journal_Info/'
extension = 'csv'
os.chdir(path)
JCR_report = [i for i in glob.glob('*.{}'.format(extension))]
print JCR_report

# Feed JCR reports into CSV reader
ISSN_number = []
i = 0
for x in JCR_report:

    # Opening Each JCR report
    myString = ''.join(x)
    year = myString[:4]
    f = open(myString)
    csv_f = csv.reader(f)

    # Print file opened
    print "Year and Topic"
    print myString

    # Create a search term from ISSN number and file name
    for row in csv_f:
        ISSN_number.append(row[2])
    ISSN_number = ISSN_number[2:-2]
    ISSN_number= [x + ") AND PUBYEAR IS " + year + " AND DOCTYPE(ar)" for x in ISSN_number]
    ISSN_number= ["ISSN(" + x for x in ISSN_number]


    # Search Scopus with each Search Term
    count = 0
    for y in ISSN_number:
        print "Journal Search"
        print y
        print count
        myString = "".join(y)
        # Retry if connection with server is lost
        check = 0
        while check < 100:
            check += 1
            try:
                # Scopus Package has been altered to change the ouput directory to Week8 Data folder
                # Output file is a .txt file with list of EIDs
                ab = ScopusSearch(myString, refresh=True)
                break
            except Exception as e:
                print(e.message)

                continue
        count += 1

    ISSN_number =[]
    i = i + 1

