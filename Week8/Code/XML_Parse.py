#!/usr/bin/python

""" This program imports results from EID_Search.py. Parses Information of authorship and ISSN number"""

__author__ = 'Thomas ELliott (t.elliott@imperial.ac.uk)'

__version__ = '0.0.1'

#Imports

import os
import glob
import re
from itertools import chain
import csv


# Changes directory and makes a list of all file names

path = '../Data/XML'
extension = 'xml'
os.chdir(path)
XML = [i for i in glob.glob('*.{}'.format(extension))]

# Converts XML to string
i = 0

author_out = []

for x in XML:
    import xml.dom.minidom
    Test_file = open(x,'r')
    xml = xml.dom.minidom.parse(Test_file)
    xml_as_string = xml.toprettyxml()
    Test_file.close()

    # Regex to find Surname
    Author_surname = []
    surname = re.findall(r'\urname>(.*?)\<',xml_as_string)
    surname = surname[2:]
    for x in surname:
        a = x.split(' ', 1)[0]
        a = a.encode('utf-8')
        Author_surname.append(a)
    Author_surname = Author_surname[1::2]


    # Regex to find Given name
    Author_first = []
    first = re.findall(r'\given-name>(.*?)\<',xml_as_string)
    first = first[1:]
    for x in first:
        a = x.split(' ', 1)[0]
        a = a.encode('utf-8')
        Author_first.append(a)
    Author_first = Author_first[1::2]

    # Regex to find ISSN number
    ISSN_number = []
    w = re.findall(r'\issn>(.*?)\<', xml_as_string)
    for x in w:
        a = x.encode('utf-8')
        ISSN_number.append(a)
    ISSN_number = ISSN_number * len(Author_surname)

    # Publication name
    Pub_name = []
    t = re.findall(r'\ublicationName>(.*?)\<', xml_as_string)
    for x in t:
        a = x.encode('utf-8')
        Pub_name.append(a)
    Pub_name = Pub_name * len(Author_surname)


    # Create EID entry
    EID_number=[XML[i]] * len(Author_surname)

    # Add Author Position
    Position = range(0,len(Author_surname))
    last_author = "LA"
    Position = Position[:-1]
    Position.append(last_author)

    # Add Year
    Year = []
    Y = re.findall(r'\overDate>(.*?)\<', xml_as_string)
    for x in Y:
        a = x[0:4]
        a = a.encode('utf-8')
        Year.append(a)
    Year = Year * len(Author_surname)

    # clean up
    if len(Author_first) != len(Author_surname):
        Author_first =["NA"] * len(Author_surname)

    # Create a table of author jornal information

    authorship=[]
    authorship.append(Author_first)
    authorship.append(Author_surname)
    authorship.append(ISSN_number)
    authorship.append(Pub_name)
    authorship.append(EID_number)
    authorship.append(Position)
    authorship.append(Year)

    res = zip(*list(chain.from_iterable(zip(authorship))))
    for x in res:
        author_out.append(x)

    i += 1


# Create CSV file
print author_out
with open('../Authorship_info.csv', 'wb') as f:
    writer = csv.writer(f)
    writer.writerows(author_out)
