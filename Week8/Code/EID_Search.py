#!/usr/bin/python

""" This program imports results from Article_Search.py. Searches scopus with EIDs to generate XML files which includes Author and Journal information"""

__author__ = 'Thomas ELliott (t.elliott@imperial.ac.uk)'

__version__ = '0.0.1'

#Imports
import csv
# Scopus Package has been altered to change the ouput directory to Week8 Data folder
from scopus.scopus_api import ScopusAbstract
import os
import glob

# Need to run shell script to add file extentions on each of the ISSN number files
# get author for each publication. Input is Scopus EID


path = '../Data/EIDs/'
extension = 'txt'
os.chdir(path)
EIDs = [i for i in glob.glob('*.{}'.format(extension))]
print len(EIDs)
print EIDs

# Feed EIDS for each search term into csv reader

EIDs_number =[]
i = 0
count = 0
for x in EIDs:
    myString = ''.join(x)
    print myString
    f = open(myString)
    csv_f = csv.reader(f)
    for row in csv_f:
        EIDs_number.append(row)
    i += 1

count = 0

# Search Author information from each EID number

for y in EIDs_number:

    print "Journal and Year"
    print x
    print "Article"
    print count
    myString = "".join(y)
    #Retry if connection with server is lost
    check = 0
    while check < 100:
        check += 1
        try:
            # Scopus Package has been altered to change the ouput directory to Week8 Data folder
            # Output is XML file
            ab = ScopusAbstract(myString, refresh=False)
            break
        except Exception as e:
            print(e.message)
            continue
    count += 1
EIDs_number=[]
