Week 1 README.txt
Thomas Elliott

Code


UniPrac1.txt		
PRACTICAL 1: Code to run on the files found in Documents/CMEECoursework/Week1/Data/FastaCode. Part 1) Count how many lines are in each file 2) Print everything starting from the second line for the E.coli genome 3) Count the sequence length of this genome 4) Count the matches of a particular sequence, “ATGC” in the genome of E. coli 5) Compute the AT/GC ratio.

boilerplate.sh		
First shell script. Prints " This is a shell script!".

tabtocsv.sh		
transforms comma seperated files to tab seperated files.

variables.sh 		
An example of values to variables assignment. Allows you to input a new string and allows you to add two numbers.

MyExampleScript.sh	
An example of values to variables assignment. Combines two strings into a single message. 

CountLines.sh		
Counts how many lines are in a file

CocatenateTwoFiles.sh	
Merges two files

FirstExample.tex	
LaTeX example. Includes abstract, introduction, materials & methods and bibliography

FirstBiblio.bib		
bibliography for FirstExample.tex

FirstExample.bbl	
Document created following the creation of MyFirstExample.pdf

First Example.blg	
Document created following the creation of MyFirstExample.pdf

FirstExample.pdf	
PDF of MyFirstExample.tex

CompileLatex.sh		
bash shell script to compile latex with bibtex

csvtospace.sh		
PRACTICAL 2: shell script that takes a comma seperated values and converts it to a space seperated values file.



Data


FastaCode		
Renamed from silbiocompmasterepo FASTA file. Contains all FASTA files needed for Practical 1.

spawannxs.txt		
A list of protected species from the UN website

Temperatures 		
Taken from silbiocompmasterepo. Contains temperature informtion files required for Practical 2.



Sandbox

Contains test and practice files


















