#!/bin/bash
# Author: Thomas Elliott t.elliott16@imperial.ac.uk
# Script: CompileLaTeX.sh
# Desc: Compiles LaTeX with bibtex!
# Arguments: none
# Date: Oct 2015

pdflatex $1.tex
pdflatex $1.tex
bibtex $1
pdflatex $1.tex
pdflatex $1.tex
evince $1.pdf &

## Cleanup
*∼
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc
