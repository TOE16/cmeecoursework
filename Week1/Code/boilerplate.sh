#!/bin/bash
#Author: Thomas Elliott TOE16@imperial.ac.uk
#Script: boilerplate.sh
#Desc: simple boilerplate for shell scripts
#Arguments: none
#Date: Oct 2015

echo -e "\nThis is a shell script! \n" # -e means exit immediately if a command exits with a non-zero status.

#exit
