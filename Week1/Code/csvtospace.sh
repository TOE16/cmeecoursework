#!/bin/bash
# Author: Thomas Elliott t.elliott16@imperial.ac.uk
# Script: UnixPrac1.txt
# Desc: Converts comma seperated values and converts it into space seperated values
# Arguments: none
# Date: Oct 2015
echo "Creating a space seperated version of $1"

cat $1 | tr -s "," " " >> $1.txt

echo " Finished! "

exit
