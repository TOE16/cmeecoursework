#!/bin/bash
#Author: Thomas Elliott TOE16@imperial.ac.uk
#Script: tabtocsv.sh
#Desc: subsitute the tabs in the files with commas
#		saves te output into a .csv file
#Arguments: 1-> tab delimited file
#Date: Oct 2015

echo "Creating a comma delimited version of $1 ..."

cat $1 | tr -s "\t" "," >> $1.csv

echo "Done!"

exit

