#!/usr/bin/python

""" descrete-time version of LV model. """

__author__ = 'Thomas Elliott TOE16@imperial.ac.uk'
__version__ = '0.0.1'

import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import sys # system specific parrameters and functions



# Define parameters:
r = 1. # Resource growth rate
a = 0.1 # Consumer search rate (determines consumption rate) 
z = 1.5 # Consumer mortality rate
e = 0.75 # Consumer production efficiency
k = 35 # prey density
Rt = 10 # prey starting number
Ct = 10 # pred starting number
t = 100 # time points

pops = sc.array([[0, Rt, Ct]]) # create an array

for timepoint in range(t): # loop to load array with new Rt and Ct values
	Rt1 = (Rt * (1 + r * ( 1 - (Rt / k) ) - a * Ct))
	Ct1 = (Ct * (1 - z + e * a * Rt))
	pops = sc.append(pops, [[timepoint + 1, Rt1, Ct1]], axis = 0)
	Rt = Rt1
	Ct = Ct1

""" Plotting LV model"""

t, prey, predators = pops.T # What's this for? taking pops and transposing it
f1 = p.figure() #Open empty figure object
p.plot( prey, 'g-', label='Resource density') # Plot
p.plot( predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics')
p.figtext(0.25, 0.125, " Parameters: \n r = %f ; a = %f ; \n z = %f; e = %f' ; k = %f" %(r,a,z,e,k)) # add parameter text to figure
#p.show()
f1.savefig('../Results/LV4.pdf') #Save figure


