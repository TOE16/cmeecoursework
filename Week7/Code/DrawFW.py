#!/usr/bin/python

"""plt.colis('all

Plot a dnapshot of a food web graph/network.

Needs: Adjacency list of who eats whom (consumer name/id in 1st column, resource name/id in 2nd column), 
and list of species names/ids), and list of species names/ids and properties as biomass(node bundance) or average body mass.

"""
import networkx as nx
import scipy as sc
import matplotlib.pyplot as plt

def GenRdAdjList (N=2, C = 0.5):
	"""
	Generate random adjacency list given N nodes with connectance
	probability C
	"""
	Ids = range(N) # generate list
	ALst = [] # creating an empty list
	for i in Ids:
		if sc.random.uniform(0,1,1) < C: # uniform dist. create link between everything else
			Lnk = sc.random.choice(Ids,2).tolist()
			if Lnk[0] != Lnk[1]: # avoid slelf loops - i interacts with self
				ALst.append(Lnk)
	return ALst
	
##Assign body mass range
SizRan = ([-10,10]) # use log scale 

## Assign number of species (MaxN) and connectance (C)
MaxN =30
C = 0.75

##Generate adjanceny list:
AdjL = sc.array(GenRdAdjList(MaxN, C))

## Generate species (node) data:
Sps = sc.unique(AdjL) # get species ids
Sizs = sc.random.uniform(SizRan[0], SizRan [1], MaxN) # Randomly generate body sixes (log10 scale)


## the plotting ##
plt.close('all')

## plot using networkx:

## Calsulate coordinates or circular configuration:
## (see networks. layout for inbuilt functions to compute other types of node
# coords)
pos = nx.circular_layout(Sps)
G=nx.Graph()
G.add_nodes_from(Sps)
G.add_edges_from(tuple(AdjL))
NodSizs = 10**-32 + (Sizs-min(Sizs))/(max(Sizs)) # node sizes in proportion to body sizes
nx.draw(G, pos, node_size = NodSizs*1000)
plt.show()
