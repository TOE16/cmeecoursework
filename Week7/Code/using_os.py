#! /usr/bin/python

""" Practical: using subprocesses to find all directories and files that begin with C and/ or c"""

__author__ = 'Thomas Elliott TOE16@imperial.ac.uk'
__version__ = '0.0.2'

# Use the subprocess.os module to get a list of files and  
# Hint: look in subprocess.os and/or subprocess.os.path and/or 
# subprocess.os.walk for helpful functions

import subprocess
import os


#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

# Type your code here:

# Get the user's home directory.
home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
FilesDirsStartingWithC = []
b =[]

# Use a for loop to walk through the home directory.

for (dir, subdir, files) in subprocess.os.walk(home):
	for i in subdir:
		if i.startswith("C"):
			FilesDirsStartingWithC.append(i)
	for d in files:
		if d.startswith("C"):
			FilesDirsStartingWithC.append(d)

print '[%s]' % ', '.join(map(str, FilesDirsStartingWithC))
	
#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'

# Create a list to store the results.
FilesDirsStartingWithCOrc = []

# Type your code here:

for (dir, subdir, files) in subprocess.os.walk(home): 
	for i in subdir:
		if i.startswith("C") or i.startswith("c"):
			FilesDirsStartingWithCOrc.append(i)
	for d in files:
		if d.startswith("C") or d.startswith("c"):
			FilesDirsStartingWithCOrc.append(d)
	

print '[%s]' % ', '.join(map(str, FilesDirsStartingWithCOrc)) # converts to string for better presentation

#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

DirsStartingWithCOrc = []

# Type your code here:

for (dir, subdir, files) in subprocess.os.walk(home):
	for i in subdir:
		if i.startswith("C") or i.startswith("c"):
			DirsStartingWithCOrc.append(i)
		
	
print '[%s]' % ', '.join(map(str, DirsStartingWithCOrc)) 

""" Prints total results found for each individual question"""

print "The number of files and directories in home that start with an uppercase 'C': %f" % (len(FilesDirsStartingWithC))

print "The number of files and directories in home that start with an uppercase 'C' or lowercase 'c': %f" % (len(FilesDirsStartingWithCOrc))

print "The number of directories in home that start with an uppercase 'C' or lowercase 'c': %f" % (len(DirsStartingWithCOrc))
