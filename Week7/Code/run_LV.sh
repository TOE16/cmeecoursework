#!/bin/bash
#Author: Thomas Elliott TOE16@imperial.ac.uk
#Script: boilerplate.sh
#Desc: run both LV1.py and LV2.py with appropriate arguments, script  also profiles the two scripts and print the results.
#Arguments: PDF Plots of both LV1.py and LV2.py outputs
#Date: Nov 2016

ipython -m cProfile LV1.py

ipython -m cProfile LV2.py 1 0.1 1.5 0.75 100

ipython -m cProfile LV3.py

ipython -m cProfile LV4.py

ipython -m cProfile LV5.py

ipython -m cProfile LV6.py
