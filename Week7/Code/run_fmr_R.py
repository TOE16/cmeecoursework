#! /usr/bin/python

"""PRACTICAL: Runs Rscript fmr.R by using subprocess"""

__author__ = 'Thomas Elliott TOE16@imperial.ac.uk'
__version__ = '0.0.1'

import subprocess
import os.path 

subprocess.call("Rscript fmr.R" ,shell=True) 

if os.path.isfile("../Results/species.csv"):
	print "species.csv has saved in Results "
	
if os.path.isfile('../Results/fmr_plot.pdf'):
	print "fmr_plot.pdf has saved in Results "
