#!/usr/bin/python

""" Demonstarting regex expressions"""

import re

my_string = "a given string"
# find a space in the string
match = re.search(r'\s', my_string) # r tells python to read teh regex in its raw form. \s match a whitespace

print match
# this should print something like
# <_sre.SRE_Match object at 0x93ecdd30>. shows that something is matched
# now we can see what has matched

match.group() # shows what is matched ' '

match = re.search(r's\w*', my_string) # s is literal so will print all alphaneumeric characters (\w*) following s

# this should return "string"
match.group()

# NOW AN EXAMPLE OF NO MATCH:
# find a digit in the string
match = re.search(r'\d', my_string) # matches a numeric character

# this should print "None"
print match

# Further Example
#
my_string = 'an example'
match = re.search(r'\w*\s', my_string) # everything leading up to the space

if match:
	print 'found a match:', match.group()
else:
	print 'did not find a match'

MyStr = 'an example'

match = re.search(r'\w*\s', MyStr) 

if match:
	print 'found a match:', match.group()
else:
	print 'did not find a match'
	
#~ #Some Basic Examples
match = re.search(r'\d' , "it takes 2 to tango") #\d prints digit
print match.group() #print 2

match = re.search(r'\s\w*\s', ' once upon a time')# finds the characters within the first and second space
match.group() # ' upon '

match = re.search(r'\s\w{1,3}\s', 'once upon a time') # w {1,3} finds characters =>1 but 3=<
match.group() # ' a '

match = re.search(r'\s\w*$', 'once upon a time') $ matches end of the line
match.group() # 'time'

match = re.search(r'\w*\s\d.*\d', 'take 2 grams of H2O')# \d.* finds all characters following the number till last number. 
match.group() # 'take 2 grams of H2'

match = re.search(r'^\w*.*\s', 'once upon a time') # matches all the characters until the last space
match.group() # 'once upon a '

# NOTE THAT *, +, and {} are all 'greedy':
#They repeat the previous regex toxen as many times as possible
#As a result, they may match more than you want

#To make it non-greedy, use?:
match = re.search(r'^\w*.*?\s', 'once upon a time')

#To further illustrate greedines, let's try matching an HTML tag:
match = re.search(r'<.+>', 'This is a <EM>first</EM> test') # finds all characters between <>
match.group() # '<EM>first</EM> '
#But we didn't want this: we wanted just <EM>
## It's because + is greedy!

#~ ## Instead we can make + "lazy!"!
match = re.search(r'<.+?>', 'This is a <EM>first</EM> test')
match.group() # '<EM>'

#~ ## OK, moving on form greed and laziness
match = re.search(r'\d*\.?\d*','1432.75+60.22i') #note "\" before "." #
print match.group() #1432.75

match = re.search(r'\d*\.?\d*','1432+60.22i')
match.group() # '1432'

match = re.search(r'[ATGC]+', 'the sequence ATTCGT')
match.group() #ATTCGT'

re.search(r'\s+[A-Z]{1}\w+\s\w+', 'The bird-shit frog''s name is Theloderma asper'). group()
#'Theloderma asper'
#Note that I directly returned the result by appending .group()
