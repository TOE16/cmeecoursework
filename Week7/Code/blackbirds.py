#!/usr/bin/python

""" Blackbird Practical for Week 7 using Regex expressions """

__author__ = 'Thomas Elliott TOE16@imperial.ac.uk'
__version__ = '0.0.2'

import re

# Read the file
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
text = str(text.decode('ascii', 'ignore'))

# Now write a regular expression my_reg that captures # the Kingdom, 
# Phylum and Species name for each species and prints it out neatly:


# Hint: you will probably want to use re.findall(my_reg, text)...
# Keep in mind that there are multiple ways to skin this cat! 

## Search for words after kingdom, Phylum and Species

Kingdom = re.findall(r'(?<=\bKingdom\s)[\w]+', text)
Phylum = re.findall(r'(?<=\bPhylum) [\w]+', text)
Species = re.findall(r'(?<=\bSpecies) [\w]+\s[\w]+', text)


# zip returns a list of tuples from each of the argument species 

a = zip(Kingdom, Phylum, Species)

# print Kingdom, Phylum and Species

print "Kingdom, Phylum and Species of blackbirds data set:"

for x in a:
	print x
        
