#!/usr/bin/python

""" Runs the Lotka-Volterra model with prey density dependence rR(1 − K/R ) """

__author__ = 'Thomas Elliott TOE16@imperial.ac.uk'
__version__ = '0.0.1'

import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import sys # system specific parrameters and functions

# import matplotlip.pylab as p #Some people might need to do this
#!/usr/bin/python
""" Take arguments for the four LV model parameters r, a, z, e from the commandline using the sys.argv function """

r = float(sys.argv[1])
a = float(sys.argv[2])
z = float(sys.argv[3])
e = float(sys.argv[4])
k = float(sys.argv[5])

def dCR_dt(pops, t=0):
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    
    R = pops[0]
    C = pops[1]
    dRdt = r*R*(1 - R/k)- a*R*C 
    dCdt = -z*C + e*a*R*C
    
    return sc.array([dRdt, dCdt])

# Define parameters:
#~ r = 1. # Resource growth rate
#~ a = 0.1 # Consumer search rate (determines consumption rate) 
#~ z = 1.5 # Consumer mortality rate
#~ e = 0.75 # Consumer production efficiency

# Now define time -- integrate from 0 to 15, using 1000 points:
t = sc.linspace(0, 200,  1000)

x0 = 10
y0 = 5 
z0 = sc.array([x0, y0]) # initials conditions: 10 prey and 5 predators per unit area

pops, infodict = integrate.odeint(dCR_dt, z0, t, full_output=True)

infodict['message']     # >>> 'Integration successful.'

""" Plotting LV model"""

prey, predators = pops.T # What's this for? taking pops and transposing it
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics')
p.figtext(0.25, 0.125, " Parameters: \n r = %f ; a = %f ; \n z = %f; e = %f' ; k = %f" %(r,a,z,e,k)) # add parameter text to figure
#p.show()
f1.savefig('../Results/LV2.pdf') #Save figure
