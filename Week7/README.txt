Week 6 README.txt
Thomas Elliott

regexs.py has been included in this weeks directory, although it was completed in a previous week. 

######Code 

blackbirds.py
PRACTICAL: using regex expressions to print out the kingdom, phylum and species of a data set.

DrawFW.py
Plots a snapshot of a food web graph/network using the networkx python package

fmr.R
Plots log(field metabolic rate) against log(body mass) for the Nagy et al 1999 dataset to a file fmr.pdf.Writes the sorted list of species names to species.csv saved in results directory.

LV1.py
Uses scipy integrate to solve Lotka-Volterra for a predator-prey system. Output plot saved in results.

LV2.py
PRACTICAL: Based upon LV1, but modified to take five LV model parameters r,a,z,e,k and runs the LV model with prey density dependence. Out put plot saved in results with parameter values added

LV3.py
PRACTICAL: LV model with parameters that both prey and predator persist with prey desnity dependence.Outputs plot to the Resuts directory with lables. Also prints final(non-zero) population value 

LV4.py
PRACTICAL: discrete-time version of LV model

LV5.py
discrete-time version of LV model with arandom gaussian fluctuation in resource’s  growth rate at each time-step

LV6.py
discrete-time version of LV model with arandom gaussian fluctuation in resource’s and consumer's growth rate at each time-step

Nets.R
PRACTICAL: networks in R where You had to change the colour of the nodes to match the type of node

profileme.py
illustrative programme to demonstrate profiling

regexs.py
Includes a number of annotated regex examples

run_fmr_R.py
PRACTICAL: Runs Rscript fmr.R by using subprocess

run_LV.sh
PRACTICAL: Runs and profiles all LV files

SQLite.py
Example of using SQLite with python

SQLinR.R
Using SQLite in R example

TestR.py
Using subprocesses to run R scripts

TestR.R
R script used in the example of using subprocesses in python to run R. Prints Hello, this is R. Has output files that save in results

timeitme.py
Script to demonstrate quick profiling by using the timeit function

using_os.py
PRACTICAL: using subprocesses to find all directories and files that begin with C and/ or c. Prints result in terminal

######Data

Biotraits.db
SQLite database on metabolic traits

blackbirds.txt
Blackbird taxonomic data for Blackbirds practical using subprocesses

Consumer.csv
Data on consumer metabolics and temperature used in the SQLite database

NagyEtAl1999.csv
Data used for Plots log(field metabolic rate) against log(body mass) for the Nagy et al 1999 dataset to a file fmr.pdf.Writes the sorted list of species names to species.csv saved in results directory.

QMEE_Net_Mat_edges.csv
Data needed for Nets.R from QMEE CDT collaboration network

QMEE_Net_Mat_nodes.csv
Data needed for Nets.R from QMEE CDT collaboration network

Resource.csv
Data on resource metabolics and temperature used in the SQLite database in R

TCP.csv
Used in SQLite exercises

test.db
Created for running SQLite in Python

TraitInfo.csv
Metabolic trait info used for SQLite practical

#######Results

errorFile.Rout
Produced by using subprocesses to run NonExsistScript.R in python - which doesnt exisist

fmr_plot.pdf
Plots log(field metabolic rate) against log(body mass) for the Nagy et al 1999 dataset from fmr.R

LV1.pdf
Lotka-Volterra model output plot

LV2.pdf
LV model with prey density dependence plot and parameters printed

LV3.pdf
LV model with prey density dependence plot where consumer and resource persist, parameters printed and non-zero value printed

LV4.pdf
discrete-time version of LV model

LV5.pdf
discrete-time version of LV model with arandom gaussian fluctuation in resource’s growth rate at each time-step

LV6.pdf
discrete-time version of LV model with arandom gaussian fluctuation in resource’s and consumer's growth rate at each time-step

outputFile.Rout
Produced by using subprocesses to run NonExsistScript.R in python - which doesnt exisist - empty

QMEENet.svg
Network produced from Nets.R where nodes are coloured based on type

species.csv
csv file of sorted list of species names from NagyEtAl1999.csv output from fmr.R practical

TestR_errFile.Rout
Produced by using subprocesses to run TestR.R in python

TestR.Rout
Produced by using subprocesses to run TestR.R in python prints 'Hello, this is R!'

#######SandBox

sqliteMem.py
creates a database in memory, without using the disk




