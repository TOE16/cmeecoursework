rm(list=ls())
setwd("~/Box Sync/Teaching/Introstats")
d = read.table("SparrowSize.txt", header = TRUE) # assigns table to d
str(d) # provides information about the structure of an object
names(d) # gets names of an object
head(d) # returns te first parts of an object
length(d$Tarsus) # total number of rows for name
hist(d$Tarsus) # plots a histogram of Tarsus
mean(d$Tarsus) # returns NA (Not available) - what exactly does NA mean
help(mean) # same as ?mean
mean(d$Tarsus, na.rm = TRUE) # mean. na.rm removes NA values
median(d$Tarsus, na.rm = TRUE) # median
mode(d$Tarsus) # tells us a description of the vector.Difficult with continuous data because data points very rarely occurs more than once.

par(mfrow = c(2,2)) # combines multiple plots.  mfrow=c(nrows, ncols) to create a matrix of nrows x 
hist(d$Tarsus, breaks = 3, col = "grey")
hist(d$Tarsus, breaks = 10, col = "grey")
hist(d$Tarsus, breaks = 30, col = "grey")
hist(d$Tarsus, breaks = 100, col = "grey")

install.packages("modeest")
require(modeest)
?modeest

mlv(d$Tarsus)
length(d$Tarsus)
length(d2$Tarsus)
mlv(d2$Tarsus)

mean(d$Tarsus, na.rm = TRUE)
median(d$Tarsus, na.rm =TRUE)
mlv(d2$Tarsus)

mean(d$Tarsus, na.rm = TRUE)

range(d$Tarsus, na.rm = TRUE)

range(d2$Tarsus, na.rm = TRUE)

var(d$Tarsus, na.rm = TRUE) # Variance

var(d2$Tarsus,na.rm=TRUE)

sum((d2$Tarsus - mean(d2$Tarsus))^2)/(length(d2$Tarsus)-1) # var equation
sqrt(var(d2$Tarsus)) # standard deviation
sd(d2$Tarsus)# also standard deviation

zTarsus <- (d2$Tarsus-mean(d2$Tarsus))/sd(d2$Tarsus) # works out z scores
var(zTarsus)
sd(zTarsus)
hist(zTarsus)

set.seed(123) # used so random values are not always the same - we can prevent this by usig a different seed each time
znormal <-rnorm(1e+06)
hist(znormal, breaks = 100)

summary(znormal) #  get centra tendencies in one go

qnorm(c(0.025,0.975))# gives us the 2.5% and 97.5% quantiles corresponding probability dist.
pnorm(.Last.value)# gets us the correpsonding probs

par(mfrow = c(1,2))
hist(znormal, breaks = 100)
abline(v = qnorm(c(0.25,0.5,0.75)), lwd = 2)
abline(v = qnorm(c(0.025,0.975)), lwd = 2, lty = "dashed")
plot(density(znormal))
abline(v = qnorm(c(0.25,0.5,0.75)), col = "gray")
abline(v = qnorm(c(0.025,0.975)), lty = "dotted", col= "black")
abline(h = 0, lwd = 3, col ="blue")
text(2, 0.3, "-1.96", col = "red", adj = 0)
text(-2,0.3,"-1.96", col ="red", adj=1)

boxplot(d$Tarsus~d$Sex.1, col=c("red", "blue"), ylab="Tarsus length (mm)")

# Type of each variable in the sparrow data set
#Bird ID - Discrete
# Year - Discrete
# Tarsus -Continuous
# Bill - Continuous
# Wing - Continuous
# Mass - Continuous
# Sex - Discrete
# Sex.1 - Discrete
par(mfrow =c(8,1))
hist(d2$BirdID, breaks =100)
hist(d2$Year, breaks =100)
hist(d2$Year, breaks =100)
hist(d2$Tarsus, breaks =100)
hist(d2$Bill, breaks =100)
hist(d2$Wing, breaks =100)
hist(d2$Mass, breaks =100)
hist(d2$Sex, breaks =100)
hist(d2$Sex.1, breaks =100)