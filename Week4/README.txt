Week 4 README.txt
Thomas Elliott

######Code

HO2.R
Averages and variance in R of Tarsus using sparrow datasets

HO4.R
Standard error in R using sparrow datasets

HO5.R
T tests in R using sparrow datasets

HO10.R
Linear models with sparrow dataset

HO13.R
ANNOVA examples with sparrow dataset

HO14.R
Replicating ANNOVA tests

HO16.R
Looking at fitting models in R that use	multiple variables to explain the response variable with daphnia dataset

HO17.R
Chi-Square test

######Data

SparrowSize.txt
Table that includes Sparrow information from Lundy Island

daphnia.txt
Dataset	on the growth of Daphnia populations, looking at the rate of growth in water containing four different detergents and using individuals of three different clones.


######Result


######Sandbox


