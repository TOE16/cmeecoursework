Week 2 README.txt
Thomas Elliott

Code

basic_io.py
Opens test.txt file in sandbox and assigns it to f. First section, python cycles over lines. Second section, skips over new lines. Third,  save elements of list to a file. Forth, saves a dictionary.

basic_csv.py
allows for easy manipulation of csv files. Uses test.csv file to list animal species pylogeney and body temp. Next section, prints pylogeny and body mass from seperate files.

boilerplate.py
Introduction to shebang, docstrings, internal variables, function definitions and modules. Prints "this is a boiler plate".

using_name.py
exemplifys how a module can be importable with the __name__=='__main__'

sys.argv.py
explains sys.argv. sys= module and argv = variable that holds arguments

scope.py
Exemplifys how variables within functions are not global, unless assigned so. Global functions are ignored within functions unless asigned so.

control_flow.py
definitions include: find whether a number is even or odd; which is the largest divisor of x among 2,3,4,5; finds whether an integer is prime;and finds all primes up to 22. Definitions can be imported individually by entering name into the terminal.

cfexercises.py
PRACTICAL: Control flow exercises: counting hellos through various for loops.

loops.py
examples of while loops - continuoulsy runs code if true

oaks.py
Examples of list comprehensions. Printing only oak species from a list of different trees. This uses set's and loops. Also, pinting lists in upper and lower cases.

lc1.py
PRACTICAL: Creating lists from bird data using list comprehensions and conventional loops. 

lc2.py
PRCATICAL: Creating lists from weather data sets using list comprehensions and conventional loops

dictionary.py
PRACTICAL: population of a dictionary with taxa data which maps order names to sets of taxa. This uses the groupby function

tuple.py
PRACTICAL: Bird data (latin name, common name, mass in tuple format. This script prints each data point on seperate lines or each species

align_seqs.py
PRACTICAL: aligns two sequences from a .csv file, finds the best alignmet with a scoring system.

align_seqs_fasta.py
PRACTICAL: aligns two FASTA files in the same manor as align_seqs.py (found in ../../Week1/Data/). Script asks user to enter FASTA file.

test_oaks.py
PRACTICAL: debugged original file. returns oak trees only from TestOakData.csv. Includes Docstring tests to ensure code runs correctly.

debugme.py
Debugging example: using odb to debug the code

Data

align_seq.csv
Contains the following seq (same as in example) seperated by "," in csv file:
seq2 = "ATCGCCGGATTACGGG"
seq1 = "CAATTCGGAT"

407228326.fasta
fasta file from week 1

407228412.fasta
fasta file from week 1

E.coli.fasta
fasta file from week 1

TestOaksData.csv
Tree names to be used for test_oaks.py practical

Results

align_seq.txt
Output file of align _seqs.py. Shows the alignment score and best match of two input DNA sequnces.

align_seq_fasta.txt

JustOaksData.csv
Result output from Practica test_oaks.py. Should only contain Oak species

Sandbox

Contains test and practice files


















