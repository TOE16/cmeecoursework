#!/usr/bin/python

""" Creating lists from bird data using list comprehensions and conventional loops"""
	
__author__ = 'Thomas Elliott (t.elliott@imperial.ac.uk)'
__version__ = '0.0.1'

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 

lc_birds_ln = [species[0] for species in birds] # assigns lc_birds_* the first data point in each tuple through a list comprehension.
print 'Bird Latin Names' # prints string 
print lc_birds_ln # prints lc_birds_ln value

lc_birds_cn = [species[1] for species in birds] # same as above but for second data point.
print 'Bird common Names'
print lc_birds_cn

lc_birds_bm = [species[2] for species in birds] # same as above but for third data point.
print 'Bird body Masses'
print lc_birds_bm

# (2) Now do the same using conventional loops (you can choose to do this 
# before 1 !). 

loop_birds_ln = [] # assigns loop_birds_* to be a list
for species in birds: # for each org in data set loop
		loop_birds_ln.append(species [0]) # appends first data point in each tuple to new list.
print 'Bird Latin Names'# prints string
print lc_birds_ln # prints list

loop_birds_cn = [] # same as above but for second data point
for species in birds: 
		loop_birds_cn.append(species [1])
print 'Bird Common Name'
print lc_birds_cn

loop_birds_cn = []# same as above but for third data point
for species in birds: 
		loop_birds_cn.append(species [2])
print 'Bird Body Masses'
print lc_birds_bm

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
