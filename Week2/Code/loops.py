"""examples of while loops - continuoulsy runs code if true"""

# for loops in python
for i in range (5):
	print i # prints 0,1,2,3,4
	
my_list = [0, 2, "geromimo!", 3.0, True, False]
for k in my_list:
	print k #prints total list
	
total = 0
summands = [0, 1, 11, 11, 1111]
for s in summands:
	print total + s # prints list
	
#~ # while loops in python
z = 0
while z < 100:
	z = z + 1 # (0 - 99) + 1
	#~ print (z) # does this for every number within 100
	
b = True
while b:
	print "GERONIMO! infinite loop! ctrl+c to stop!" # will continuously print string
	
# ctrl + c to stop!
