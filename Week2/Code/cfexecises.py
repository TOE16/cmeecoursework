#!/usr/bin/python

""" Some functions exemplifying the use of control satements"""
	
__author__ = 'Thomas Elliott (t.elliott@imperial.ac.uk)'
__version__ = '0.0.2'

import sys

# How many times will 'hello' be printed?
# 1)
"""starting from the 4th number print hello until 17"""
for i in range (3, 17) : 
	print 'hello'
	
# 2)
""" print hello for any number with remainder of zero when divided by 3 within the range of 12"""
for j in range(12) :
	if j % 3 == 0: 
		print 'hello'
		
# 3)
"""print hello for any number with remainder of 3 when divided by 5 and 4 within the range of 15"""
for j in range(15) : 
	if j % 5 == 3:
		print 'hello'
	elif j % 4 == 3:
		print 'hello'
		
# 4)
""" print hello if z does not = 15"""
z = 0 
while z != 15: 
	print 'hello'
	z = z + 3
	
# 5)
z = 12
while z < 100:
	if z == 31:
		for k in range(7) :
			print 'hello'
	elif z == 18:
			print 'hello'
	z = z + 1
		
# what does fooXX do? # any generic function
def foo1 (x) :
	return x ** 0.5
	
def foo2 (x, y):
	if x > y:
		return x
	return y
	
def foo3 (x, y, z):
	if x > y:
		tmp = y
		y = x
		x = tmp
	if y > z:
		tmp = z
		z = y 
		y = tmp
	return [x, y, z]
	
def foo4 (x) :
	result = 1
	for i in range (1, x + 1) :
		result = result * i
	return result
	
 """This is a recursive function, meaning that the function calls itself read about it at en.wikipedia.org/wiki/Recursion_(computer_science)"""
def foo5 (x) :
	if x == 1:
		return 1
		return x * foo5(x-1)

def main(argv): #
	print foo1 (2)
	print foo2 (2,3)
	print foo3 (2,3,4)
	print foo4 (1)
	print foo5 (10)
	
if (__name__=="__main__"):
		status = main(sys.argv)
		sys.exit(status)
