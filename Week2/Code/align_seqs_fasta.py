#!usr/bin/python

"""PRACTICAL: aligns two sequences from a Fasta files, finds the best 
alignmet with a scoring system"""
	
__author__ = 'Thomas Elliott (t.elliott16@imperial.ac.uk)'
__version__ = '0.0.1'


seq = raw_input('Enter FASTA seq. file name:') # Asks you to insert file
fh = open(seq,'r') # reads file and assigns fh
line = fh.readline() 
meta =''
sequence= ''
while line:
	line = line.rstrip('\n') # strips new lines
	if '>' in line:
		meta = line # identifys meta info line
	else:
		sequence = sequence + line
	line = fh.readline()

seq1 = ''.join(sequence) # converts to a string so can be taken by rest of code


seq = raw_input('Enter FASTA seq. file name:') # Asks you to insert file
fh = open(seq,'r') # reads file and assigns fh
line = fh.readline() 
meta =''
sequence= ''
while line:
	line = line.rstrip('\n')
	if '>' in line:
		meta = line
	else:
		sequence = sequence + line
	line = fh.readline()

seq2 = ''.join(sequence)	


# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest

l1 = len(seq1) 
l2 = len(seq2)
if l1 >= l2:
    s1 = seq1
    s2 = seq2
else:
    s1 = seq2
    s2 = seq1
    l1, l2 = l2, l1 # swap the two lengths

# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
    # startpoint is the point at which we want to start
    matched = "" # contains string for alignement
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            # if its matching the character
            if s1[i + startpoint] == s2[i]:
                matched = matched + "*"
                score = score + 1
            else:
                matched = matched + "-"

    # build some formatted output
    print "." * startpoint + matched           
    print "." * startpoint + s2
    print s1
    print score 
    print ""

    return score

calculate_score(s1, s2, l1, l2, 0)
calculate_score(s1, s2, l1, l2, 1)
calculate_score(s1, s2, l1, l2, 5)

# now try to find the best match (highest score)
my_best_align = None
my_best_score = -1

for i in range(l1):
    z = calculate_score(s1, s2, l1, l2, i)
    if z > my_best_score:
        my_best_align = "." * i + s2
        my_best_score = z

print my_best_align
print s1
print "Best score:", my_best_score

import io

f = open('../Results/align_seqs_fasta.txt','w')
f.write('Best score: '+ str(my_best_score) + '\n'+ 'My Best Allignment ' + '\n' + str(my_best_align) + '\n' + str(s1))
f.close
