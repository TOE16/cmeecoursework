#!/usr/bin/python # shebang - specifys the location to thepython executable in your machine that rest of the script needs to be interpreted with

""" Description of this program
	you can use several lines""" # doc string
	
__author__ = 'Thomas Elliott (t.elliott@imperial.ac.uk)' # internal variable
__version__ = '0.0.1'

# imports
import sys # module to interface our program with the operating system

# constants can go here

# functions can go here - subsequent lines must be indented
def main (argv):
		print 'This is a boilerplate' # NOTE: indented usisng two tabs or 4 spaces
		return 0
		
if (__name__ == "__main__"): #makes sure the "main" function is called from the commandline
		status = main(sys.argv)
		sys.exit(status) # way to exit abruptly
		
