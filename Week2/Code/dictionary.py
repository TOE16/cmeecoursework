#!/usr/bin/python

"""Practical: population of a dictionary with taxa data which maps order names 
to sets of taxa. This uses the groupby function """
	
__author__ = 'Thomas Elliott (t.elliott@imperial.ac.uk)'
__version__ = '0.0.1'

taxa = [ ('Myotis lucifugus','Chiroptera'),
         ('Gerbillus henleyi','Rodentia',),
         ('Peromyscus crinitus', 'Rodentia'),
         ('Mus domesticus', 'Rodentia'),
         ('Cleithrionomys rutilus', 'Rodentia'),
         ('Microgale dobsoni', 'Afrosoricida'),
         ('Microgale talazaci', 'Afrosoricida'),
         ('Lyacon pictus', 'Carnivora'),
         ('Arctocephalus gazella', 'Carnivora'),
         ('Canis lupus', 'Carnivora'),
        ]

# Write a short python script to populate a dictionary called taxa_dic 
# derived from  taxa so that it maps order names to sets of taxa. 
# E.g. 'Chiroptera' : set(['Myotis lucifugus']) etc. 

# imports
from itertools import groupby # imports the groupby function from itertools.
#The groupby() function takes two arguments: (1) the data to group and (2) the function to group it with.
import pprint # imports the pretty print module

taxa_dic = {}  # assigns taxa_dic to dictionary 

for key, group in groupby(taxa, lambda x: x[1]): # Organises the order alphabetically. lambda is just a fancy way of saying function
    taxa_dic[key] = " and ".join([order[0] for order in group])# assigns key to taxa_dic. Seperates different species by and within order. Assigns species to corresponding order.

pprint.pprint(taxa_dic) #Prints the formatted representation of taxa_dic on stream, followed by a newline
    
# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

# Write your script here:
