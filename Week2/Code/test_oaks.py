#!/usr/bin/python

""" Creating lists from bird data using list comprehensions and conventional loops"""
	
__author__ = 'Thomas Elliott (t.elliott@imperial.ac.uk)'
__version__ = '0.0.1'

import csv
import sys
import pdb
import doctest

#Define function
def is_an_oak(name): # Docstring tests
    """ Returns True if name is starts with 'quercus '
        >>> is_an_oak('quercus')
        True
        
        Returns False if name is starts with 'QQuercus '
        >>> is_an_oak('QQuercus')
        False
		
		Returns False if name is starts with 'Fagus sylvatica'
        >>> is_an_oak('Fagus sylvatica')
        False
		
    """
    return name.lower() == 'quercus' # removed space after 'quercus ' and changed startswith() to ==
    
print(is_an_oak.__doc__)

def main(argv): 
    f = open('../Data/TestOaksData.csv','rb') # changed relative path
    g = open('../Results/JustOaksData.csv','wb') #  changed relative path
    taxa = csv.reader(f)
    csvwrite = csv.writer(g)
    # 'oaks = set()' not needed
    for row in taxa:
        print row
        print "The genus is", row[0]
        if is_an_oak(row[0]):
            print row[0]
            print 'FOUND AN OAK!'
            print " "
            csvwrite.writerow([row[0], row[1]])    
    
    return 0
    
if (__name__ == "__main__"):
    status = main(sys.argv)

doctest.testmod()
