"""Exemplifys how variables within functions are not global, unless assigned so. Global functions are ignored within functions unless asigned so."""

_a_global = 10

def a_function():
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None

a_function()
print "Outside the function, the value is ", _a_global

_a_global = 10
def a_function():
	global _a_global # assigns _a_global within a function to be global
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None
	
a_function()
print "Outside the function, the value is", _a_global
