"""allows for easy manipulation of csv files. Uses test.csv file to list animal species pylogeney and body temp. Next section, prints pylogeny and body mass from seperate files."""

import csv

# Read a file containing:
# 'Species','Infraorder','Family','Distribution','Body mass male (Kg)'
f = open('../Sandbox/testcsv.csv','rb')

csvread = csv.reader(f)
temp = [] # assigns temp the value between []
for row in csvread:
	temp.append(tuple(row)) # ammends temp value to row
	print row # prints row
	print "The species is", row[0] # prints 'The species is followed by the first part of the row.
	
f.close()

# write a file containing only species name and Body mass
f = open('../Sandbox/testcsv.csv','rb') #rb = readable binary - assigns to f
g = open('../Sandbox/bodymass.csv','wb') # wb = writable binary - assigns to g

csvread = csv.reader(f)
csvwrite = csv.writer(g)
for row in csvread:
	print row # prints f
	csvwrite.writerow([row[0], row[4]])# ammends body mass
	
f.close()
g.close()
