#!/usr/bin/python

""" Some functions exemplifying the use of control statements"""
	
__author__ = 'Thomas Elliott (t.elliott@imperial.ac.uk)'
__version__ = '0.0.1'

# imports
import sys # module to interface our program with the operating system
import doctest # Impor the doctest module

# constants can go here
def even_or_odd(x=0): # if not specified, x should take value 0.
	"""Find whether a number x is even or odd.
	
	>>> even_or_odd(10)
	'10 is Even!'
	
	>>> even_or_odd(5)
	'5 is Odd!'
	
	whenever a float is provided, then the closest integer is used:
	>>> even_or_odd(3.2)
	'3 is Odd!'
		
	in the case of negative numbers, the positive is taken:
	>>> even_or_odd(-2)
	'-2 is Even!'
	
	"""
	
	# Define a function to be tested
	if x % 2 == 0: #The conditional if
		return "%d is Even!" % x
	return "%d is Odd!" % x

#~ def main(argv):
	#~ # sys.exit("don't want to do this right now!")
	#~ print even_or_odd(22)
	#~ print even_or_odd(33)
	#~ return 0	
		
#~ if (__name__=="__main__"): #makes sure the "main" function is called from the commandline
		#~ status = main(sys.argv)
		
				
doctest.testmod() # to run with embedded tests
