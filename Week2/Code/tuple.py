#!/usr/bin/python

""" Practical: Bird data (latin name, common name, mass in tuple format. 
This script prints each data point on seperate lines or each species"""
	
__author__ = 'Thomas Elliott (t.elliott@imperial.ac.uk)'
__version__ = '0.0.1'


birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
        )

# Birds is a tuple of tuples of length three: latin name, common name, mass.
# write a (short) script to print these on a separate line for each species
# Hints: use the "print" command! You can use list comprehension!

for tu in birds: # loops through for each tuple
    print tu [0] # prints 1st column in tuple
    print tu [1] # prints 2nd column in tuple on new line 
    print tu [2] # prints 3rd column in tuple on new line


# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
