#!/usr/bin/python
# Filename: using_name.py

"""exemplifys how a module can be importable with the __name__=='__main__'"""

if __name__ == '__main__':
	print 'This program is being run by itself'
else:
	print 'I am being imported from another module'
