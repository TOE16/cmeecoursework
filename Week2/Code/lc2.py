#!/usr/bin/python

""" Creating lists from weather data sets using list comprehensions and conventional loops"""
	
__author__ = 'Thomas Elliott (t.elliott@imperial.ac.uk)'
__version__ = '0.0.1'

# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )

# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.

lc_rainfall_hun = [rain for rain in rainfall if rain[1] > 100] # list comprehension where each value in column 2 are above 100 is identified. These tuples are assigned to lc_rainfall_hun
print 'Months where rainfall was greater than 100 mm'# prints Months where rainfall was greater than 100 mm'
print lc_rainfall_hun # prints lc_rainfall_hun = months where rain is above 100mm
 
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

lc_rainfall_fif = [rain for rain in rainfall if rain[1] < 50]
print 'Months where rainfall was less than 50 mm'# list comprehension where each value in column 2 are bellow is identified. These tuples are assigned to lc_rainfall_fif
print lc_rainfall_fif # prints lc_rainfall_fif = months where rain is below 50mm

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !).

loop_rainfall_hun = []
for rain in rainfall : # loop 
	if rain [1] > 100: #where where each value in column 2 are above 100 is identified. These tuples are assigned to loop_rainfall_hun
		loop_rainfall_hun.append(rain) # adds rain to loop_rainfall_hun list
print 'Months where rainfall was greater than 100 mm' # prints Months where rainfall was greater than 100 mm'
print loop_rainfall_hun  # prints loop_rainfall_hun = months where rain is above 100mm

loop_rainfall_fif = []
for rain in rainfall : # loop 
	if rain [1] < 50:  #where where each value in column 2 are below 50 is identified. These tuples are assigned to loop_rainfall_fif
		loop_rainfall_fif.append(rain)# adds rain to loop_rainfall_hun list
print 'Months where rainfall was less than 50 mm' # prints Months where rainfall was below 50 mm'
print loop_rainfall_fif # prints loop_rainfall_fif = months where rain is below 50mm

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
