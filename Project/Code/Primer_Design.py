# ~ #!usr/bin/python
""" Converting T-DNA CSV into a easy search db """

__author__ = 'Thomas Elliott (t.elliott16@imperial.ac.uk)'
__version__ = '0.0.1'

#import primer3
import pandas as pd

TDNALibrary = pd.read_csv("T-DNA-insertions-V3-assembly-3-18-2016.csv")

SplitTDNA = TDNALibrary.str.split('[,](?!\s)').apply(pd.Series)

TDNALibrary.to_csv("Output.csv")

#SequencesToPrimer = pd.read_csv("../../Line_Selection/Results/AllSitesForPrimers.csv")
#
#PDATA = pd.DataFrame([])
#
#for index, row in SequencesToPrimer.iterrows():
#    Sequence = row ['TotalRange']
#    SequenceID = row ['Site']
#    Primers = primer3.bindings.designPrimers(
#    {
#        'SEQUENCE_ID': SequenceID,
#        'SEQUENCE_TEMPLATE': Sequence,
#        'SEQUENCE_INCLUDED_REGION': [1,4000]
#    },
#    {
#        'PRIMER_OPT_SIZE': 20,
#        'PRIMER_MIN_SIZE': 18,
#        'PRIMER_MAX_SIZE': 25,
#        'PRIMER_OPT_TM': 60.0,
#        'PRIMER_MIN_TM': 57.0,
#        'PRIMER_MAX_TM': 63.0,
#        'PRIMER_MIN_GC': 20.0,
#        'PRIMER_MAX_GC': 80.0,
#        'PRIMER_MAX_POLY_X': 100,
#        'PRIMER_INTERNAL_MAX_POLY_X': 100,
#        'PRIMER_SALT_MONOVALENT': 50.0,
#        'PRIMER_DNA_CONC': 50.0,
#        'PRIMER_MAX_NS_ACCEPTED': 0,
#        'PRIMER_MAX_SELF_ANY': 12,
#        'PRIMER_MAX_SELF_END': 8,
#        'PRIMER_PAIR_MAX_COMPL_ANY': 12,
#        'PRIMER_PAIR_MAX_COMPL_END': 8,
#        'PRIMER_PRODUCT_SIZE_RANGE': [[1501,4000]],
#        'SEQUENCE_INTERNAL_EXCLUDED_REGION':[[1500,1000]],
#        'SEQUENCE_TARGET': [[1500,1000]],
#    })
#    
#    a = pd.DataFrame(Primers)
#    a = a.drop(a.index[:1])
#    a['Site'] = [SequenceID]
#    PDATA = PDATA.append(a)
#    a = 0
#
#
#
#PDATA.to_csv("../Results/PrimersDF.csv")

    

