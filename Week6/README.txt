Week 6 README.txt
Thomas Elliott

Practical sheets which included questions are stored in Sandbox. ME_analysis includes many files necessary for practical 3. These couldn't be stored in the defauslt CMEE format so have left them as a seperate directory.

######Code

BasicPopGenomics.R
R script generated following Basic population genomics Using R. Includes Hardy Weinberg Equib analysis, plotting genotype on allele frequencies, plotting expected vs observed heterozygosity, finding specific loci that are large departures

######Data

H938_chr15.geno
Genetic data file required to run practical 2.

primate_aligned.fasta
FASTA aligned sequences which are similar to primate mtDNA data from Hodgson et al.2009

primate_raw.fasta
FASTA raw sequences which are similar to primate mtDNA data from Hodgson et al.2009

######ME_Analysis

Includes all files for in-class practical 3, organised as asked from practical worksheet. 

######Results

ChangesChromo.pdf
Plot from practical 2:finding specific loci that are large departures from the HW

CountOfSNPs.pdf
Plot from practical 2: plot shows count of SNPs from

ExObHet.pdf
Plot from practical 2:expected vs observed heterozygosity

FIshers.pdf
Plot from practical 2: tests Hardy Weinberg Probability

FreqA2A1.pdf
Plot from practical 2:frequency of the major allele (A2) vs freq of minor allele (A1)

gTidy.pdf
Plot from practical 2:genotype over allele freq

HWgTidy.pdf
Plot from practical 2:genotype over allele freq including Hardy Weinberg 

MovingAvg.pdf
Plot from practical 2: local average in a sliding window of SNPs across the genome

######Sandbox
1_Data_and_databases_2016.docx
In-class practical 1

2_Basic_pop_genomics_2016.docx
in-class practical 2

3_Pop_Structure_Migration_2016.docx
in-class practical 3

