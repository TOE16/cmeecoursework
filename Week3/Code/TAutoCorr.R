##Is the temperaure of one year significantly correlated with the next year, across the year.

rm(list = ls()) # removes data in the environment
load("../Data/KeyWestAnnualMeanTemperature.RData") # load data frame

# Step 1:to compare temperature correlation between n-1 pair of years, 
# I have made two lists that have been truncated so each year is lined up with its following year in a row.

MyDF<- data.frame(ats$Temp) # Creates MyDF which is populated with Temp data
MyDFT <- data.frame(ats$Temp) # Creates MyDF which is populated with Temp data
MyDF <- MyDF[-c(1), ] # removes first row from MyDF
MyDFT <- MyDFT[-nrow(MyDFT),] # Removes last row

#Calculate coefficent
CorTC<-cor(MyDFT, MyDF, method="pearson") # Calculates correlation coefficient using pearsons as variables are linear

# Step 2: Repeat the calculation 10000 times

Repeat10 <- replicate (10000,{ # repeats function and saves this in Repeat10
                      sample(MyDF,replace = T, prob = NULL)->CMyDF #randomizes MyDF
                      CorRepeat10<-cor(MyDFT, CMyDF, method="pearson") # works out the correlation
}
  )
R <- data.frame(Repeat10) # Changes Repeat10 into a dataframe
R <- t(R) # transposes dataframe

# Calculating how many of the what fraction of the correlation coefficients from step 2 were greater
#than that from step 1 (this is the approximate p-value).

a <-length( which( R > CorTC )) # calculates how many corrlation coefficients from step 2 were greater than that from step 1
b<- a/10000 # raction of the correlation coefficients from step 2 were greaterthan that from step 1 (this is your approximate p-value).
print("Aprroximate P Value")
print(b)
