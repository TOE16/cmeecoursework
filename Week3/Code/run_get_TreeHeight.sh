#!/bin/bash
#Author: Thomas Elliott TOE16@imperial.ac.uk
#Script: boilerplate.sh
#Desc: tests get_TreeHeight. R for practical 7.14
#Arguments: none
#Date: Nov 2016

Rscript get_TreeHeight.R ../Data/trees.csv
