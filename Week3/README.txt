Week 2 README.txt
Thomas Elliott

#########Code

basic_io.R
Imports trees.csv with headers into MyData.csv. Writes row names, but ignores column names.

boilerplate.R
Creates a Function and tests the function

TreeHeight.R
Practical: Creates a function that calculates the height of trees from the ange of elevation and the distance from the base using trigonometric formula height = distance * tan(radians)

get_TreeHeight.R
takes a csv file from the command line and includes name in output file. Requires Input fle

run_get_TreeHeight.sh
Shell script that test get_TreeHeight.R using trees.csv as an example file.

control.R
Includes a number of control statements using if, then , else.

PP_Lattice.R 
saves three lattice graphs by feeding interaction type: one of predator mass , one of prey mass and one of the size ratio of prey mass overpredator mass in PDF in Results folder. Also script calculates the mean and median log predator mass, prey mass, and predator-prey size ratio, by feeding type, and save it as a single csv output table called PP_Results.csv to the Results directory.

DataWrang.R
Originally copied from silbiorepo. Example of data wrangling of the Pound Hill data.

Vectorize1.R
An example of Vectorization and how it can speed up code.

apply1.R
apply vectorizes code. apply1.R is an an example of this!

apply2.R
using apply to define own functions.

break.R
an example on of how to break out of loops.

next.R
Skip to next iteration of a loop. The script checks if a number is odd using the "modulo" operation and prints if it is.

Vectorize2.R
Originally taken from bit bucket repository. Edited with vectorisation techniques to run faster.

sample.R
Script that randomly samples from a generated population.

try.R
using try function to catch errors and keep going

browse.R
an example of hoow to carry out debugging step by step

TAutoCorr.R
Using the tem-perature in Key West, Florida for the 20th century data set Is the temperature of one year significantly correlated with the next year (successive years), across the years. This script calculates the appropriate correlation coefficient between successive years. Then repeats this calculation 10000 times by randomly permuting the time series. Then calculates what fraction of the correlation coefficients from step 2 were greater than that from step 1 (this is your approximate p-value).

GPDDFiltered_RData_GISMap.R
Loads Maps package superimposes GPDD data over world map. Includes comment on what biases might you expect in any analysis based on the datarepresented?


###########Data

trees.csv
Includes tress species with their corresponding distance (m) and angle (degrees)

EcolArchives-E089-51-D1.csv
Predator and Prey species information used for Data wrangling exercises and practicals.

PoundHillData.csv
Pound Hill data collected in by students in the Silwood Field Course week.

PoundHillMetaData.csv
Meta Pound Hill data collected in by students in the Silwood Field Course week.

Results.txt
Data matrix used for Case study 3.

Resource.csv
From bitbucket. Used in chapter 9.6.1 example of SQlite with R

KeyWestAnnualMeanTemperature.Rdata
This is the temperature in Key West, Florida for the 20th century.

GPDDFiltered.RData
Contains locations of animal species used for Practical


###########Results

TreeHts.csv
Practical: TreeHts.csv contains data from trees.csv but also includes calculated height.

Pred_Prey_Overlay.pdf
Histogram showing Predator-Prey Size Overlap.

MyData.csv
Includes tress species with their corresponding distance (m), angle (degrees) and calculated height

Pred_Lattice.pdf
Lattice graph by feeding interaction type for Predator Mass

Prey_Lattice.pdf
Lattice graph by feeding interaction type for Prey Mass

SizeRatio_Lattice.pdf
Lattice graph by feeding interaction type for Prey/Predator Mass

PP_Results.csv
Stores mean and median log predator mass, prey mass, and predator-prey size ratio, by feeding type fromPP_Lattice.R

MyFirst-ggplot2-Figure.pdf
Predator-prey mass ratios with log axis rom pound hill data 

Girko.pdf
Case study 2: plotting two dataframes. Result of simulation of Girkos circular law.

MyBars.pdf
Case study 3: Overlay of three lineranges and a text geometry from Results.txt data

MyLinReg.pdf
Case study 4: Mathematical display graph. Linear regression with colors expressing residuals and mathematical annotations.

PP_Regress_Results.csv
script that draws and saves a pdf file of PP_Regress_output.R : linear regression of Predator mass/ Prey mass with Predator. lifestage legend and by Type of feeding interaction.

PP_Regress_Loc.csv
script that draws and saves a pdf file of PP_Regress_output.R : linear regression of Predator mass/ Prey mass with Predator. lifestage legend and by Type of feeding interation and by location.

TAutoCorr.pdf
Includes code form TAutoCorr.R and comment of results. Compiled by Latex

GPDDFiltered_RData_GISMap.pdf
World map with layer of all the locations from which we have data in the GPDD dataframe.

########SandBox

CompiLatex.sh
Latest Latex compiler

TAutoCorr.tex
template for latex for practical


