#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -l select=1:ncpus=1:mem=800mb
module load R/
echo "R is about to run"
R --vanilla < $WORK/toe16_HPC.R
mv toe16_* $WORK
echo "R has finised running"

